#include "Fibo.hpp"
#include <iostream>
#include "assert.h"

int main() {
    for (int i=0; i<50; i++) {
        //assert(fibo(i) >= 0); //desactivé avec -DNDEBUG
        if(fibo(i) <=0 )
            throw (std::string)"erreur resultat négatif !";
        std::cout << fibo(i) << std::endl;
    }
    return 0;
}

/**
a partir des itérations 47 le type INT ne permet pas de supporter la taille des nombres calculés ce qui porvoque l'erreur
 qui affiche des nombres négatifs
 **/