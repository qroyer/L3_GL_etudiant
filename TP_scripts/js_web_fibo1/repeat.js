"use strict";

function repeatN(n,f) {
    const list = document.createElement("ul");
    for(let i = 0 ; i < n ; i ++)
    {
        const node = document.createElement("li");
        node.innerHTML = f(i);
        console.log(f(i));
        list.appendChild(node);
    }
    return list;
}