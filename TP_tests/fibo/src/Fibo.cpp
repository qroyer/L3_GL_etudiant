#include <string>
#include "Fibo.hpp"

int fibo(int n, int f0, int f1) {
    if(f0 > f1){
        throw (std::string) "error invalid parameters";
    }
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

